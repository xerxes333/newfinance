<?php

namespace common\models;

use yii\db\ActiveRecord;


class Transaction extends ActiveRecord
{
	public $stamp;
	public $total;
	public $title;
	public $color;
	public $info;
    public $tagList;
    public $duplicate;
	
	public static function tableName()
    {
        return 'transactions';
    }
    
    public function scenarios()
    {
        return [
            'import' => ['raw_date', 'data', 'amount', 'tagList', 'duplicate'],
        ];
    }
	
	public function getTags()
	{
		return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
			->viaTable('transaction_tags', ['transaction_id' => 'id']);
	}
	
	public static function monthlyTotals($tag = null)
	{
		if(isset($tag)){
			$rows = self::find()
				->select(["concat(year(raw_date),'-',month(raw_date),'-1') AS stamp", "ABS(SUM(amount)) AS total"])
				->joinWith('tags')
				->where('tags.abbr=:abbr', [':abbr'=>$tag])
				->groupBy('stamp')
	    		->orderBy('stamp')
				->all();
		} else {
			$rows = self::find()
				->select(["concat(year(raw_date),'-',month(raw_date),'-1') AS stamp", "ABS(SUM(amount)) AS total"])
				->where('amount < :amt', [':amt'=>0])
				->groupBy('stamp')
	    		->orderBy('stamp')
				->all();
		}
		
		
		
		// create array with all dates since jan 2009
		$start 	= strtotime("01 January 2009");
		$end 	= strtotime(date("1 F Y"));
		$i 		= $start;
		
		while($i <= $end) {
			$allMonths[date("Y-n-j",$i)] = 0;
			$i = strtotime("+1 month",$i);
		}
		
		foreach ($rows as $row) {
			$dumbRows[$row->stamp] = $row->total;
		}
		
		$merged = array_merge($allMonths, $dumbRows);
		
		// now convert dates to UTC dates for JSON
		foreach ($merged as $key => $value) {
			$json[] = array((float)(date("U",strtotime($key)) * 1000)	, (double)$value);
		}
			
		return $json;
		
	}

	public static function monthTotals($month = null, $year = null){
		
		$month 	= (isset($month) and is_numeric($month))? $month : date("n");
		$year 	= (isset($year) and is_numeric($year))? $year : date("Y");
		$start 	= "$year-$month-01";
		$end 	= "$year-$month-" . date("t", mktime(1,1,11,$month,1,$year));
		$arr 	= [];
		
		$rows = self::find()
				->select(["tags.title", "ABS(SUM(amount)) AS total", "tags.color"])
				->joinWith('tags')
				->where('raw_date BETWEEN :start AND :end', [':start'=>$start, ':end'=>$end])
				->groupBy('tags.id')
				->all();
				
		
		foreach ($rows as $row) {	
			$arr[] = array(
				'name' => $row->title, 
				'data' => [(int)$row->total],
				'color'=> $row->color,
				'month'=> $month,
				'year' => $year,
			);
			
		}
		
		return $arr;
		
	}
    
    public function isDup(){
        // check transactions table for record that has same raw_date and amount
        $count = Transaction::find()
                ->where(['raw_date' => date('Y-m-d', strtotime($this->raw_date)), 'amount' => $this->amount])
                ->count();
        if($count>0)
            return TRUE;
        else
            return FALSE;
    }
    
    public function descFilter($str = ''){
        $this->data = preg_replace('/\"|check crd purchase|pos purchase|PURCHASE AUTHORIZED ON [0-9].\/[0-9]. | - | \?mcc\=[0-9]*|[0-9]+x{6}+[0-9]*|[A-Z][0-9]{5,22}|RECUR DEBIT CRD PMT|x{3,12}|\'|CARD [0-9]{4}|Mach ID| 000000 /i', '', $str);
    }
    
    // TODO: Need a more elegant to do this  
    public function tagsFilter($str = ''){
        $list = [];
        
        switch ($str) {
            // GROCERY
            case (preg_match('/Harris Teeter/i', $str) ? TRUE: FALSE):
            case (preg_match('/HARRIS TE/i', $str) ? TRUE: FALSE):
            case (preg_match('/Earth Fare/i', $str) ? TRUE: FALSE):
            case (preg_match('/TRADER JOE/i', $str) ? TRUE: FALSE):
            case (preg_match('/whole foods/i', $str) ? TRUE: FALSE):
            case (preg_match('/THE MEATHOUSE/i', $str) ? TRUE: FALSE):
            case (preg_match('/THE MEATHOUSE/i', $str) ? TRUE: FALSE):
            case (preg_match('/SAMSCLUB/i', $str) ? TRUE: FALSE):
            case (preg_match('/SAMS CLUB/i', $str) ? TRUE: FALSE):
                $list[] = 1;
                break;
            // DINE OUT
            case (preg_match('/ZEITOUNI/i', $str) ? TRUE: FALSE):
            case (preg_match('/LUNAS LIVING/i', $str) ? TRUE: FALSE):
            case (preg_match('/MONTERREY/i', $str) ? TRUE: FALSE):
            case (preg_match('/BONEHEADS/i', $str) ? TRUE: FALSE):
            case (preg_match('/BRICKTOPS/i', $str) ? TRUE: FALSE):
            case (preg_match('/MACHU PICCHU/i', $str) ? TRUE: FALSE):
            case (preg_match('/BANGKOK OCHA/i', $str) ? TRUE: FALSE):
            case (preg_match('/DENNYS/i', $str) ? TRUE: FALSE):
            case (preg_match('/DON PEDRO/i', $str) ? TRUE: FALSE):
            case (preg_match('/DEAN \& DELUCA/i', $str) ? TRUE: FALSE):
            case (preg_match('/DILWORTH NEIGH/i', $str) ? TRUE: FALSE):
            case (preg_match('/BOB EVANS/i', $str) ? TRUE: FALSE):
            case (preg_match('/NOODLE/i', $str) ? TRUE: FALSE):
            case (preg_match('/MELLOW MUSHROOM/i', $str) ? TRUE: FALSE):
            case (preg_match('/BAD DADDY/i', $str) ? TRUE: FALSE):
            case (preg_match('/CRACKER B/i', $str) ? TRUE: FALSE):
            case (preg_match('/ZAPATAS/i', $str) ? TRUE: FALSE):
            case (preg_match('/WALDHORN/i', $str) ? TRUE: FALSE):
            case (preg_match('/BUFFALO WILD WINGS/i', $str) ? TRUE: FALSE):
            case (preg_match('/PIO PIO/i', $str) ? TRUE: FALSE):
            case (preg_match('/CHIPOTLE/i', $str) ? TRUE: FALSE):
            case (preg_match('/TROPICAL CAJUN ATLANTA GA/i', $str) ? TRUE: FALSE):
            case (preg_match('/APPLEBEES/i', $str) ? TRUE: FALSE):
            case (preg_match('/TACO BELL/i', $str) ? TRUE: FALSE):
                $list[] = 2;
                break;
            // GB
            case (preg_match('/PETSMART/i', $str) ? TRUE: FALSE):
            case (preg_match('/PROVIDENCE VETERIN/i', $str) ? TRUE: FALSE):
                $list[] = 4;
                break;
            // BILLS
            case (preg_match('/state farm/i', $str) ? TRUE: FALSE):
            case (preg_match('/sprint.*wire/i', $str) ? TRUE: FALSE):
            case (preg_match('/bill.*pay/i', $str) ? TRUE: FALSE):
            case (preg_match('/TIME WARNER/i', $str) ? TRUE: FALSE):
            case (preg_match('/LA FITNESS/i', $str) ? TRUE: FALSE):
            case (preg_match('/URBAN ACTIVE/i', $str) ? TRUE: FALSE):
            case (preg_match('/VERIZON WRLS/i', $str) ? TRUE: FALSE):
            case (preg_match('/VERIZON WRL/i', $str) ? TRUE: FALSE):
            case (preg_match('/STATE FARM/i', $str) ? TRUE: FALSE):
                $list[] = 6;
                break;
            // GAS
            case (preg_match('/SHELL SERVICE/i', $str) ? TRUE: FALSE):
            case (preg_match('/EXXON/i', $str) ? TRUE: FALSE):
            case (preg_match('/PETRO/i', $str) ? TRUE: FALSE):
            case (preg_match('/SUNOCO/i', $str) ? TRUE: FALSE):
            case (preg_match('/GULF OIL/i', $str) ? TRUE: FALSE):
            case (preg_match('/KANGAROO EX/i', $str) ? TRUE: FALSE):
            case (preg_match('/COBBLESTONE CREE/i', $str) ? TRUE: FALSE):
            case (preg_match('/MARKET EXPRESS FORT MILL SC/i', $str) ? TRUE: FALSE):            
            case (preg_match('/MARKET EXPRESS 360 FORT MILL SC/i', $str) ? TRUE: FALSE):            
                $list[] = 7;
                break;
                
            //INCOME
            case (preg_match('/PAYCHECK/i', $str) ? TRUE: FALSE):
            case (preg_match('/INSP LLC DIR DEP/i', $str) ? TRUE: FALSE):
                $list[] = 9;
                break;
                
            // MOVIES
            case (preg_match('/CINEMAS/i', $str) ? TRUE: FALSE):
            case (preg_match('/REDBOX/i', $str) ? TRUE: FALSE):
                $list[] = 11;
                break;
            // COFFEE
            case (preg_match('/STARBUCKS/i', $str) ? TRUE: FALSE):
            case (preg_match('/CARIBOU/i', $str) ? TRUE: FALSE):
                $list[] = 14;
                break;
            // BB.COM
            case (preg_match('/BODYBUILDING.COM/i', $str) ? TRUE: FALSE):
            case (preg_match('/BODYBUILDIN/i', $str) ? TRUE: FALSE):
                $list[] = 5;
                break;
            // TARGET
            case (preg_match('/TARGET/i', $str) ? TRUE: FALSE):
                $list[] = 26;
                break;
            // Paintball
            case (preg_match('/PAINT ROCKHILL/i', $str) ? TRUE: FALSE):
                $list[] = 3;
                break;
                
            default:
                # code...
                break;
        }

        $this->tagList = implode(',', $list);
    }
    
}




















