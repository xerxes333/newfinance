<?php

namespace common\models;

use yii\db\ActiveRecord;


class Tag extends ActiveRecord
{
	public $stamp;
	public $total;
	
	public static function tableName()
    {
        return 'tags';
    }
	
}