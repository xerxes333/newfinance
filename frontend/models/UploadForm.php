<?php

namespace frontend\models;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * UploadForm is the model behind the upload form.
 */
class UploadForm extends Model
{
    /**
     * @var UploadedFile file attribute
     */
    public $file;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['file'], 'required'],
        	[['file'], 'file'],
            // [['file'], 'file', 'checkExtensionByMimeType' => FALSE, 'extensions' => 'csv', 'mimeTypes' => 'text/csv, text/plain, application/csv, application/x-csv, text/comma-separated-values'],
        ];
    }
}

?>