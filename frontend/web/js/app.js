
var months 	= ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var monthsUC= ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

function populateMonthGraph(obj){
		
	var d = new Date(parseInt(obj.x));
	var m = d.getMonth();
	var y = d.getFullYear() ;
	var i = 0;
	var ret;
	
	var chart2 = Highcharts.charts[1];
	obj.color = 'red';
    $.ajax({
		url: '/index.php?r=site/json&func=monthdata&m='+(m+1)+'&y='+y,
		success: function(data, textStatus, jqXHR) {
			spinner();
			title = months[m] + ' ' + y;
			getDetails(m,y);
			redrawMonthDetails(title, data);
		},
		error: function(jqXHR, textStatus, errorThrown){
			console.log(errorThrown);
		},
		dataType: 'json'
	});
    
}

function redrawMonthDetails(title, data){
	$('#highcharts-4').highcharts({
		legend: {
			layout: 'vertical',
			align: 'left',
			borderWidth: 0,
			verticalAlign: 'top',
			reversed: true
		},
    	chart: {
    		type: 'bar',
    		style: {fontFamily: 'sans-serif'}
    	},
    	title: {
    		text: title
    	},
    	xAxis: {
    		categories: '',
    		labels: {
    			enabled: false
    		}
    	},
    	tooltip: {
    		headerFormat: '',
			footerFormat: '',
			valueDecimals: 2,
			useHTML: true,
			enabled: true,
			followPointer: true,
    	},
    	plotOptions:{
    		bar: {
    			groupPadding: 0,
    			pointPadding: 0.1
    		},
    		series: {
    			borderWidth: 0,
				dataLabels: {
					enabled: true,
					rotation:0,
					align: 'left',
					formatter: function(){
	       				return this.y + ": " + this.series.name;
	       			}
				},
				point: {
					events: {
						click: function(){
							spinner();
							var m = this.series.options.month - 1;
							var y = this.series.options.year;
							var t = this.series.name;
							getDetails(m, y, t);
						}
					}
				}
    		}	
    	},
    	
    	
    	series: data
    });
}

function totalsTooltip(obj){
	
	var d = new Date(obj.x);
	var m = d.getMonth();
	var y = d.getFullYear() ;

	return months[m] + ' ' + y + ' $'+ obj.y + '';
}

function getDetails(m,y,t){
	
    $.ajax({
		url: '/index.php?r=site/details&m='+ m +'&y='+ y +'&t='+ t,
		success: function(data, textStatus, jqXHR) {
			$('#gridview-details').html(data);
		},
		error: function(jqXHR, textStatus, errorThrown){
			console.log(errorThrown);
		},
		dataType: 'html'
	});
	
}

function spinner(){
	$('#gridview-details').html('<span class="btn-lg glyphicon glyphicon-refresh glyphicon-spin"></span>');
}




