<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Transaction;
use common\models\Tag;
use frontend\models\UploadForm;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\bootstrap\Alert;

/**
 * Site controller
 */
class SiteController extends Controller
{
	/**
	 * TODO
	 * 
	 * - Unify the way months are handled.  Right now  some methods are zero based and others are 1 based.
	 */
	 
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /************************************
     *          ACTION METHODS          *
     ************************************/
    public function actionIndex()
    {
    	
        $transactions = Transaction::find()
			->where('amount < 0')
    		->orderBy('raw_date');
		
		// Monthly Totals
		$total = Transaction::monthlyTotals();
		$series[] = [
			'name' 	=> 'Monthly Total',
			'color'	=>'#999999', 
			'data'	=>$total
		];
		
		
		// Monthly Totals Grouped By Tag
		$tags = Tag::find()->all();
		foreach ($tags as $tag) {
			$tran = Transaction::monthlyTotals($tag->abbr);
			$series[] = [
				'name'		=> $tag->title,
				'visible'	=> false, 
				'color'		=> $tag->color, 
				'data'		=> $tran
			];
		}
		
		$mon = Transaction::monthTotals(1,2009);
		
        return $this->render('index', [
        	'transactions' => $transactions,
        	'series'       => $series,
        	'tran'         => $tran,
        	'ser'          => $mon,
        	'details'      => $this->actionDetails(0,2009)
        ]);
    }

	public function actionJson($func = null, $m = null, $y = null){
		
		switch ($func) {
			case 'monthdata':
				$mon = Transaction::monthTotals($m,$y);
				\Yii::$app->response->format = 'json';
				return $mon;
				break;
			
			default:
				// do nothing
				break;
		}
	}
	
	public function actionDetails($m = null, $y = null, $t = null){
		
		$m    = $m+1;
		$start= "$y-$m-01";
		$end  = "$y-$m-" . date("t", mktime(1,1,11,$m,1,$y));
		$t    = (strtolower($t) == 'undefined')? null : $t;
		
        if(!empty($t)){
            $transactions = Transaction::find()->joinWith('tags')
                ->where("tags.title = '{$t}' AND raw_date BETWEEN '{$start}' AND '{$end}'")
                ->orderBy('raw_date ASC');
        } else {
            $transactions = Transaction::find()->with('tags')
                ->where("raw_date BETWEEN '{$start}' AND '{$end}'")
                ->orderBy('raw_date ASC');
        }
        
		
		$dataProvider = new ActiveDataProvider([
		    'query' => $transactions,
		    'pagination' => [
		        'pageSize' => 0,
		    ],
		]);
		
		$mon = GridView::widget([
		    'dataProvider' => $dataProvider,
		    'id' => 'gridview-details',
		    'columns'=>[
		    	'raw_date', 
		    	[
		    		'header' => 'Name',
		    		'format' => 'html',
		    		'value' => function($data){
		    			return $this->detailLabels($data);
		    		}
		    	],
		    	'amount'
	    	],
		]);
		
		return $mon;
		
	}
	
	public function actionImport()
	{
		$model = new UploadForm();
		$showUpload = TRUE;
		
		if (Yii::$app->request->isPost) {
			
            $showUpload = FALSE;
            
            // If we are uploading a csv file
            if(!empty(Yii::$app->request->post('UploadForm'))){
                $handle = $this->uploadFile($model);
                $transactions = $this->getFileTransactions($handle);
            }
            
            // If we are adding tags to records
            if(!empty(Yii::$app->request->post('Transaction'))){
                
                $result = $this->importTransactions(Yii::$app->request->post('Transaction'));     
                
                // TODO: could word the alerts better to check for singular/plural
                // TODO : put the conditional checks into a method  
                if(!empty($result['noTags'])){
                    Yii::$app->getSession()->setFlash('warning', count($result['noTags']) . " transactions need tags");
                    $transactions = $result['noTags'];
                }
                  
                if(!empty($result['isDup'])){
                    foreach ($result['isDup'] as $key => $tran) {
                        $transactions[] = $tran;
                    }
                    
                    Yii::$app->getSession()->setFlash('danger', count($result['isDup']) . " transactions are duplicate and will be ignored");
                }   
                
                if($result['success'] > 0){
                    Yii::$app->getSession()->setFlash('success', $result['success'] . " transactions imported successfully");
                }
                
            }
        
        }
		
        $tags = Tag::find()->orderBy('abbr')->all();
        
		return $this->render('import', [
        	'transactions' => $transactions,
        	'model'        => $model,
        	'tags'         => $tags,
        	'showUpload'   => $showUpload,
        ]);
		
        
	}
	    
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    
    
    
    /************************************
     *          HELPER METHODS          *
     ************************************/
    public function detailLabels($data){
        foreach ($data->tags as $tag) {
            $label .= '<span class="label" style="background-color:'. $tag->color .';">&nbsp;&nbsp;</span> ';           
        }
        return  $label . $data->data;
    }
    
    private function importTransactions($data){
        
        $noTags = [];
        $isDup  = [];
        $count  = 0;
        
        foreach ($data as $key => $tran) {
            
            $transaction = new Transaction;
            $transaction->scenario = 'import';
            $transaction->attributes = $tran;
            
            if(empty($transaction->tagList)){
                $noTags[] = $transaction;
            } else {
                if($transaction->isDup() && $tran['force'] != TRUE){
                    $transaction->duplicate = TRUE;
                    $isDup[] = $transaction;
                } else {
                    $transaction->save();
                    
                    $tags = explode(',', $tran['tagList']);
                    foreach ($tags as $t) {
                        $tag = Tag::findOne($t);
                        $transaction->link('tags',$tag);
                    }
                    
                    $count++;
                }
            }
            
        }
        
        return [
            'noTags' => $noTags,
            'isDup'  => $isDup,
            'success'=> $count,
        ];
        
    }
    
    private function uploadFile($model){
        
        $model->file = UploadedFile::getInstance($model, 'file');
        
        if ($model->file && $model->validate()) {
            $fileName = $model->file->baseName . '.' . $model->file->extension;
            $model->file->saveAs('uploads/' . $fileName);
            $handle = fopen('uploads/' . $fileName, "r");
            return $handle;
        }
        return null;
    }
    
    private function getFileTransactions($handle){
        
        $transactions = [];
        
        while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
            if(isset($data[0])){
                $tran = new Transaction();
                $tran->raw_date = date('Y-m-d', strtotime($data[0]));
                $tran->descFilter($data[4]);
                $tran->amount   = $data[1];
                $tran->tagsFilter($data[4]);
                $transactions[] = $tran;
            }
            
        }
        
        return $transactions;
    }
}
