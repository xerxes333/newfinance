

<ul class="list-unstyled small" data-spy="affix">
    
    <?php foreach ($tags as $key => $tag):?>
    <li> 
        <div class="tag-square" style="background-color: <?php echo $tag->color; ?>;">
            <?php echo $tag->id; ?>
        </div> 
        <?php echo $tag->abbr; ?>
    </li>
    <?php endforeach; ?>
</ul>

