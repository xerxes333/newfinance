<?php
/* @var $this yii\web\View */
$this->title = 'Finances';

use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\Highstock;
use yii\web\JsExpression;

?>
<div class="site-index">
	
    <div class="body-content">
        
        <div class="row">
            <div class="col-lg-12">
            	
            	<?php
				
				echo Highcharts::widget([
					'scripts' => [
				        'themes/dark-unica',
				    ],
				   	'options' => [
				   		'legend' => [
					    	'layout' => 'vertical',
					    	'align' => 'left',
					    	'borderWidth' => 0,
					    	'verticalAlign' => 'top',
					    ],
				   		'chart' => [
				   			'height' => '460',
				   			'type' => 'column',
				   			'style' => ['fontFamily' => 'sans-serif'],
				   		],
				   		
						'tooltip' => [
							'formatter' => new JsExpression('function(){ return totalsTooltip(this); }')
						],
						
						
						'title' => ['text' => 'Monthly Totals'],
				        'xAxis' => ['type' => 'datetime'],
				        'plotOptions' => [
				        	'series' => [
				        		'borderWidth' => 0,
				        		'cursor' => 'pointer',
				                'point' => [
			                    	'events' => [
			                        	'click' => new JsExpression('function(){ populateMonthGraph(this); }')
				                    ]
                				],
                				'allowPointSelect' => true,
                				'states' => [
                					'select' => [
                						'color' => 'red'
                					]
                				]
				        	],
				        ],
						'series' => $series
				   	]
				]);
            	?>
            	
				
			</div>
		</div>
		
		<br>
		<div class="row">
            <div class="col-lg-6">
            	<?php
				echo Highcharts::widget([
				   	'options' => [
				   		'legend' => [
					    	'layout' => 'vertical',
					    	'align' => 'left',
					    	'borderWidth' => 0,
					    	'verticalAlign' => 'top',
					    	'reversed' => true,
					    ],
				   		'chart' => [
				   			'type' => 'bar',
				   			'style' => ['fontFamily' => 'sans-serif'],
				   		],
						'title' => ['text' => 'Jan 2009'],
				        'xAxis' => [
				        	'categories' => '',
				        	'labels' => [
				        		'enabled' => false
				        	],
			        	],
			        	'tooltip' => [
			        		"headerFormat" => '',
							"footerFormat" => '',
							"valueDecimals" => 2,
							"useHTML" => true,
							"enabled" => true,
							"followPointer" => true,
			        	],
				        'plotOptions' => [
				        	'bar' => [
				        		'groupPadding' => 0,
				        		'pointPadding' => 0.1
				        	],
				        	'series' => [
				        		'borderWidth' => 0,
				        		'dataLabels' => [
					        		'enabled' => true,
									'rotation' => 0,
									'align' => 'left',
				        			'formatter' => new JsExpression('function(){
				        				return this.y + ": " + this.series.name;
				        			}')
				        		],
				        		'point' => [
                                   'events' => [
                                       'click' => new JsExpression('function(){
                                            var m = this.series.options.month - 1;
                                            var y = this.series.options.year;
                                            var t = this.series.name;
                                            getDetails(m, y, t);
                                            spinner();
                                        }')
                                   ]
                                ]
				        	]
				        ],
						'series' => $ser
				   	]
				]);
            	?>
            	
			</div>
			
			<div class="col-lg-6">
				<div style="height: 400px; overflow: scroll; overflow-x: hidden; padding: 4px;">
	            	<?php
	            	$dataProvider = new ActiveDataProvider([
					    'query' => $transactions,
					    'pagination' => [
					        'pageSize' => 0,
					    ],
					]);
					
					echo $details;
					
	            	?>
            	</div>
			</div>
		</div>
		
    </div>
</div>
