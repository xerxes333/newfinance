<?php
/* @var $this yii\web\View */
$this->title = 'Finances';

use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\widgets\Alert;

$this->context->layout = 'import';
?>
<div class="site-index">
	
    <div class="body-content">
        
        <div class="row">
            <div class="col-lg-11">
            	
            	<?= Alert::widget() ?>
            	
            	<?php
            	
            	if(!$showUpload){
                    $dataProvider = new ArrayDataProvider([
                        'allModels' => $transactions,
                        'pagination' => FALSE,
                        'sort' => FALSE
                    ]);
                    
                    
                    $form = ActiveForm::begin();
                    
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'rowOptions' =>  function($data){
                            $class = '';
                            if($data->amount >= 0)  $class = 'success';
                            if($data->duplicate)    $class = 'danger';
                            
                            if(preg_match('/TARGET/i', $data->data))    $class = 'danger';
                            if(preg_match('/WAL-MART/i', $data->data))    $class = 'info';
                            return ['class' => $class]; 
                        },
                        'columns'=> [
                            [
                                'label' => 'Date',
                                'format' => 'raw',
                                'value' => function($data, $key){
                                    $force = ($data->duplicate)? Html::checkbox("Transaction[$key][force]", FALSE, ['label'=>'Force?']) : '';
                                    return $data->raw_date . Html::hiddenInput("Transaction[$key][raw_date]",$data->raw_date, ['class'=>'form-control']) . $force;
                                }
                            ],
                            [
                                'label' => 'Description',
                                'format' => 'raw',
                                'value' => function($data, $key){
                                    return Html::textInput("Transaction[$key][data]",$data->data, ['class'=>'form-control']);
                                },
                                'contentOptions'=>['style'=>'width: 70%;']
                            ],
                            [
                                'label' => 'Amount',
                                'format' => 'raw',
                                'value' => function($data, $key){
                                     return $data->amount ." ". Html::hiddenInput("Transaction[$key][amount]",$data->amount, ['class'=>'form-control']);
                                }
                            ],
                            [
                                'label' => 'Tags',
                                'format' => 'raw',
                                'value' => function($data, $key){
                                    return Html::textInput("Transaction[$key][tagList]",$data->tagList, ['class'=>'form-control', 'tabindex'=>'1']);
                                    // return '';
                                }
                            ]
                        ]
                    ]);
                    
                    
                    echo "<button class=\"btn btn-primary\"> <span class=\"glyphicon glyphicon-export\" aria-hidden=\"true\"></span>&nbsp; Import</button>";
                    
                    ActiveForm::end();
                }
                
            	?>
            	
            	
                <?php if($showUpload): ?>
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class'=>'form']]) ?>                 
                    
                    <?= $form->field($model, 'file')->fileInput() ?>
                    <button class="btn btn-primary"> <span class="glyphicon glyphicon-floppy-open" aria-hidden="true"></span> Submit</button>
                <?php ActiveForm::end() ?>
                <?php endif; ?>
                
				
			</div>
			<div class="col-lg-1">
                <?= $this->render('_tags',['tags' => $tags]) ?>
            </div>
		</div>

    </div>
</div>
