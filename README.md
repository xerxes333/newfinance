New Finances (Yii 2 Framework)
===================================

In this project I am moving many of my old scripts from as far back as 2006 into a unified platform. 
At the same time I am exploring the new features of the Yii 2 Framework.
The scripts are for importing and tracking my finances so I can look for ways to better and more efficient ways to manage my finances.
I only run this software on my local machine and is never on a public server so I can overlook many of the inherent security issues with a website.

INSTALLATION
------------
Follow the Yii 2 installation instructions [Here](http://www.yiiframework.com/doc-2.0/guide-start-installation.html)


GETTING STARTED
---------------

After you install the application, you have to conduct the following steps to initialize
the installed application. You only need to do these once for all.

1. Run command `init` to initialize the application with a specific environment.
2. Create a new database and adjust the `components['db']` configuration in `common/config/main-local.php` accordingly.
3. Apply migrations with console command `yii migrate`. This will create tables needed for the application to work.
4. Set document roots of your Web server:
5. Create additional tables in the database (see SQL below)

- for frontend `/path/to/yii-application/frontend/web/` and using the URL `http://frontend/`
- for backend `/path/to/yii-application/backend/web/` and using the URL `http://backend/`

To login into the application, you need to first sign up, with any of your email address, username and password.
Then, you can login into the application with same email address and password at any time.
 

ADDITIONAL TABLES
---------------

```
#!sql

CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `color` varchar(15) DEFAULT NULL,
  `abbr` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `transaction_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `raw_date` date DEFAULT NULL,
  `data` varchar(255) DEFAULT NULL,
  `details` text,
  `amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `temp_trans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `raw_date` date DEFAULT NULL,
  `data` varchar(255) DEFAULT NULL,
  `details` text,
  `amount` decimal(10,2) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
```
